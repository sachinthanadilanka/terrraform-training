const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 8080;

app.use(bodyParser.json());

// Sample route to get a message
app.get('/api', (req, res) => {
    res.json({ message: 'Hello, World!' });
  });

app.get('/api/results', (req, res) => {
  res.json({ message: 'Hello, World!' });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
