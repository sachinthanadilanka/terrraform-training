const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 8080;

app.use(bodyParser.json());

// Sample route to get a message
app.get('/api/customer', (req, res) => {
    res.json({ message: 'List of Customers' });
  });

app.get('/api/customer/:id', (req, res) => {
  res.json({ message: `Customer Info of Customer ID: ${req.params.id}` });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
