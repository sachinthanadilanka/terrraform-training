variable "vpc_id" {
  description = "VPC ID"
}

variable "private_route_table_id" {
  description = "Private route table id"
}

variable "aws_region" {
  
}