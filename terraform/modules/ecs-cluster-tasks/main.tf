##############################
# Data
##############################
data "aws_subnet_ids" "app_subnets" {
  vpc_id = var.vpc_id

  filter {
    name   = "tag:Domain"
    values = ["private"]
  }

  filter {
    name   = "tag:Environment"
    values = ["dev"]
  }
}

##############################
# ECS Resources
##############################
resource "aws_ecs_cluster" "app_engine_cluster" {
  name = "app_engine_cluster"

    setting {
      name = "containerInsights"
      value = "enabled"
    }
}

# Define an IAM role for ECS tasks (customize policies as needed)
data "aws_iam_policy_document" "ecs_agent" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_agent" {
  name               = "ecs-agent"
  assume_role_policy = data.aws_iam_policy_document.ecs_agent.json
}


# Create a security group for ECS tasks (customize rules as needed)
resource "aws_security_group" "ecs_sg" {
  name        = "ecs-sg"
  description = "ECS SecurityGroup"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "ALL"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ECS SecurityGroup"
  }
}

# resource "aws_iam_instance_profile" "ecs_agent" {
#   name = "ecs-agent"
#   role = aws_iam_role.ecs_agent.name
# }

# Attach necessary policies to the ECS task execution role
resource "aws_iam_role_policy_attachment" "ecs_ecr_policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs_agent.name
}

resource "aws_iam_role_policy_attachment" "ecs_ec2_policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
  role       = aws_iam_role.ecs_agent.name
}

resource "aws_iam_role_policy_attachment" "ecs_ecr_policy_correct" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSAppRunnerServicePolicyForECRAccess"
  role       = aws_iam_role.ecs_agent.name
}

# resource "aws_iam_role_policy_attachment" "ecs_kms_policy" {
#   policy_arn = "arn:aws:iam::aws:policy/service-role/ROSAKMSProviderPolicy"
#   role       = aws_iam_role.ecs_agent.name
# }


resource "aws_cloudwatch_log_group" "aggregation_api_service_tasks_lg" {
  name = "/ecs/aggregation-service"
  tags = {
    Name = "Aggregation api service log group"
  }
}


# task definition for aggregation_api_service
# resource "aws_ecs_task_definition" "aggregation_api_service" {
#   family                   = "aggregation-api"
#   network_mode             = "awsvpc"    # Specify your network mode if different
#   requires_compatibilities = ["FARGATE"] # Use "EC2" if you are using EC2 instances

#   execution_role_arn = aws_iam_role.ecs_agent.arn # Replace with the ARN of your ECS execution role

#   memory = 2048                                                                        # Mandatory: Set the memory required by the container in MiB
#   cpu    = 1024                                                                        # Mandatory: Set the CPU units required by the container

#   container_definitions = jsonencode([{
#     name   = "aggregation-api"
#     image  = "071837351618.dkr.ecr.ap-southeast-2.amazonaws.com/aggregation-api:latest" # Mandatory: Replace with your Docker image repository URI and tag

#     runtime_platform = {
#         operating_system_family = "LINUX"
#         # cpu_architecture        = "X86_64"
#         cpu_architecture        = "ARM64"
#     }

#     portMappings = [{
#       containerPort = 8080 # Mandatory: Replace with the port your application listens on
#       hostPort      = 8080 # Mandatory: Replace with the port you want to map to on the host
#     }],

#     logConfiguration = {
#       logDriver = "awslogs"
#       options = {
#         awslogs-group : "/ecs/aggregation-service"
#         awslogs-region : "ap-southeast-2"
#         awslogs-stream-prefix : "ecs"
#       }
#     }
#     }]
#   )
# }

# ECS cluster service for aggregation api

# Create an internal NLB
resource "aws_lb" "nlb" {
  name               = "aggregation-service-internal-nlb"
  internal           = true  # Internal NLB
  load_balancer_type = "network"

  enable_deletion_protection = false

  subnets = data.aws_subnet_ids.app_subnets.ids
}

# Create a Cloud Map namespace
resource "aws_service_discovery_private_dns_namespace" "cloudmap_namespace" {
  name               = "app-engine"
  description        = "ECS service discovery namespace fro App Engine"
  vpc                = var.vpc_id
}

# Create a Cloud Map service
resource "aws_service_discovery_service" "aggregation_api_service_discovery" {
  name             = "aggregation-api"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.cloudmap_namespace.id
    
    dns_records {
      ttl  = 10
      type = "A"
    }
    
    routing_policy = "MULTIVALUE"  # Use "MULTIVALUE" for NLB integration
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

# Define the ECS service
# resource "aws_ecs_service" "aggregation_api_service" {
#   name            = "aggregation-service"
#   cluster         = aws_ecs_cluster.app_engine_cluster.name  # Replace with your ECS cluster name
#   task_definition = aws_ecs_task_definition.aggregation_api_service.arn
#   launch_type     = "FARGATE"  # Use "EC2" if you are using EC2 instances
#   desired_count   = 1  # Adjust as needed

#   network_configuration {
#     subnets = data.aws_subnet_ids.app_subnets.ids
#     security_groups = [aws_security_group.ecs_sg.id]
#   }

#   load_balancer {
#     target_group_arn = aws_lb_target_group.nlb_target_group.arn
#     container_name   = "aggregation-api"
#     container_port   = 8080
#   }

#   service_registries {
#     registry_arn = aws_service_discovery_service.aggregation_api_service_discovery.arn
#   }

#   health_check_grace_period_seconds = 15
# }

# Define a target group for the NLB
resource "aws_lb_target_group" "nlb_target_group" {
  name     = "aggregation-api-tg"
  port     = 8080
  protocol = "TCP"
  vpc_id   = var.vpc_id
  target_type = "ip" # Set the target_type to "ip" for NLB
}

resource "aws_lb_listener" "aggregation_service_listner" {
  load_balancer_arn = aws_lb.nlb.arn
  port              = 8080
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb_target_group.arn
  }
}


data "aws_iam_policy_document" "s3_ecr_access" {
  version = "2012-10-17"
  statement {
    sid     = "s3access"
    effect  = "Allow"
    actions = ["*"]

    principals {
      type        = "*"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id            = var.vpc_id
  service_name      = "com.amazonaws.${var.aws_region}.s3"
  vpc_endpoint_type = "Gateway"
  route_table_ids   = [var.private_route_table_id]
  policy            = data.aws_iam_policy_document.s3_ecr_access.json
}

resource "aws_vpc_endpoint" "ecr-dkr-endpoint" {
  vpc_id              = var.vpc_id
  private_dns_enabled = true
  service_name        = "com.amazonaws.${var.aws_region}.ecr.dkr"
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.ecs_sg.id]
  subnet_ids          = data.aws_subnet_ids.app_subnets.ids
}

resource "aws_vpc_endpoint" "ecr-api-endpoint" {
  vpc_id              = var.vpc_id
  service_name        = "com.amazonaws.${var.aws_region}.ecr.api"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.ecs_sg.id]
  subnet_ids          = data.aws_subnet_ids.app_subnets.ids
}

resource "aws_vpc_endpoint" "ecs-agent" {
  vpc_id              = var.vpc_id
  service_name        = "com.amazonaws.${var.aws_region}.ecs-agent"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.ecs_sg.id]
  subnet_ids          = data.aws_subnet_ids.app_subnets.ids
}

resource "aws_vpc_endpoint" "ecs-telemetry" {
  vpc_id              = var.vpc_id
  service_name        = "com.amazonaws.${var.aws_region}.ecs-telemetry"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.ecs_sg.id]
  subnet_ids          = data.aws_subnet_ids.app_subnets.ids
}

resource "aws_vpc_endpoint" "ecs-logs" {
  vpc_id              = var.vpc_id
  service_name        = "com.amazonaws.${var.aws_region}.logs"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.ecs_sg.id]
  subnet_ids          = data.aws_subnet_ids.app_subnets.ids
}

resource "aws_vpc_endpoint" "ecs" {
  vpc_id              = var.vpc_id
  service_name        = "com.amazonaws.${var.aws_region}.ecs"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.ecs_sg.id]
  subnet_ids          = data.aws_subnet_ids.app_subnets.ids
}

output "ecs_agent_role_name" {
  value = aws_iam_role.ecs_agent.name
}

#sha256:2699c9692401c1bc4c39f229e5d210f5d419bad99efd7ebc810d9981f5882cd9
#sha256:374d23021d365412e82d68810f86afe62cc66107ae8b225551c5f9c80a2effd0




# task definition for customer_api_service

resource "aws_cloudwatch_log_group" "customer_api_service_tasks_lg" {
  name = "/ecs/customer-service"
  tags = {
    Name = "Customer api service log group"
  }
}

# resource "aws_ecs_task_definition" "customer_api_service" {
#   family                   = "customer-api"
#   network_mode             = "awsvpc"    # Specify your network mode if different
#   requires_compatibilities = ["FARGATE"] # Use "EC2" if you are using EC2 instances

#   execution_role_arn = aws_iam_role.ecs_agent.arn # Replace with the ARN of your ECS execution role

#   memory = 2048                                                                        # Mandatory: Set the memory required by the container in MiB
#   cpu    = 1024   
  
#   runtime_platform = {
#     operating_system_family = "LINUX"
#         # cpu_architecture        = "X86_64"
#     cpu_architecture        = "ARM64"
#   }
#                                                                      # Mandatory: Set the CPU units required by the container

#   container_definitions = jsonencode([{
#     name   = "customer-api"
#     image  = "071837351618.dkr.ecr.ap-southeast-2.amazonaws.com/customer-api:latest" # Mandatory: Replace with your Docker image repository URI and tag

#     portMappings = [{
#       containerPort = 8080 # Mandatory: Replace with the port your application listens on
#       hostPort      = 8080 # Mandatory: Replace with the port you want to map to on the host
#     }],

#     logConfiguration = {
#       logDriver = "awslogs"
#       options = {
#         awslogs-group : "/ecs/customer-service"
#         awslogs-region : "ap-southeast-2"
#         awslogs-stream-prefix : "ecs"
#       }
#     }
#     }]
#   )
# }


# Define the ECS service
# resource "aws_ecs_service" "customer_api_service" {
#   name            = "customer-service"
#   cluster         = aws_ecs_cluster.app_engine_cluster.name  # Replace with your ECS cluster name
#   task_definition = aws_ecs_task_definition.customer_api_service.arn
#   launch_type     = "FARGATE"  # Use "EC2" if you are using EC2 instances
#   desired_count   = 1  # Adjust as needed

#   force_new_deployment = true

#   network_configuration {
#     subnets = data.aws_subnet_ids.app_subnets.ids
#     security_groups = [aws_security_group.ecs_sg.id]
#   }


# #   load_balancer {
# #     target_group_arn = aws_lb_target_group.nlb_target_group.arn
# #     container_name   = "customer-api"
# #     container_port   = 8080
# #   }

# #   service_registries {
# #     registry_arn = aws_service_discovery_service.aggregation_api_service_discovery.arn
# #   }

# #   health_check_grace_period_seconds = 15
# }