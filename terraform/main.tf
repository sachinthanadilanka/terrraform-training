terraform {
  backend "s3" {
    bucket = "sachin-terraform-test-automation"
    key    = "terraform/terraform-test"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.5.0"
    }
  }
}

variable "aws_region" {
  default = "ap-southeast-2"
}


resource "aws_budgets_budget" "monthly-budget" {
  name              = "monthly-budget"
  budget_type       = "COST"
  limit_amount      = "100"
  limit_unit        = "USD"
  time_unit         = "MONTHLY"
  time_period_start = "2023-10-01_00:01"
}


module "ecs-cluster-tasks" {
  source = "./modules/ecs-cluster-tasks"
  vpc_id = aws_vpc.my_vpc.id
  private_route_table_id = aws_route_table.private.id
  aws_region = var.aws_region
}

module "app-container-repo" {
  source = "./modules/app-container-repo"
  #   vpc_id = aws_vpc.my_vpc.id
}

module "api-gateway" {
  source = "./modules/api-gateway"
  # vpc_id = aws_vpc.my_vpc.id
}

module "bastion-host" {
  source           = "./modules/bastion-host"
  vpc_id           = aws_vpc.my_vpc.id
  public_subnet_id = "subnet-06c7e00a3d2677cca"
  private_subnet_id = "subnet-0b42f162859b0f9a5"
  ecs_agent_role_name = module.ecs-cluster-tasks.ecs_agent_role_name
}