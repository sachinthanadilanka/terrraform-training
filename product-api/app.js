const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 8080;

app.use(bodyParser.json());

// Sample route to get a message
app.get('/api/product', (req, res) => {
    res.json({ message: 'List of Products' });
  });

app.get('/api/product/:id', (req, res) => {
  res.json({ message: `Product Info of Product ID: ${req.params.id}` });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
