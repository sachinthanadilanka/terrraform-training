resource "aws_vpc" "my_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name        = "app-engine-vpc"
    Environment = "dev"
  }
}

resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id
}

resource "aws_subnet" "public_subnet" {
  count      = 3
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = element(["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"], count.index)
  availability_zone = element(
    tolist(data.aws_availability_zones.available.names),
    count.index
  )
  map_public_ip_on_launch = true

  tags = {
    Environment = "dev"
    Domain      = "public"
  }
}

resource "aws_subnet" "private_subnet" {
  count      = 3
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = element(["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"], count.index)
  availability_zone = element(
    tolist(data.aws_availability_zones.available.names),
    count.index
  )

  tags = {
    Environment = "dev"
    Domain      = "private"
  }
}


resource "aws_route_table" "public" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    Name = "public-route-table"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    Name = "private-route-table"
  }
}

resource "aws_route" "public_internet" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0" # All traffic
  gateway_id             = aws_internet_gateway.my_igw.id
}


# resource "aws_route_table" "public_route_table" {
#   count  = 3
#   vpc_id = aws_vpc.my_vpc.id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.my_igw.id
#   }
# }

# Associate public subnets with the public route table
resource "aws_route_table_association" "public_subnet_association" {
  count          = length(aws_subnet.public_subnet)
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public.id
}

# Associate private subnets with the private route table
resource "aws_route_table_association" "private_subnet_association" {
  count          = length(aws_subnet.private_subnet)
  subnet_id      = aws_subnet.private_subnet[count.index].id
  route_table_id = aws_route_table.private.id
}

# resource "aws_vpc_endpoint" "s3_gateway" {
#   vpc_id       = aws_vpc.my_vpc.id
#   service_name = "com.amazonaws.ap-southeast-2.s3"
# }

#Update routetables to configure routing for above DKR and ECR endpoints.

# resource "aws_route" "ecr_dkr_route" {
#   route_table_id         = aws_route_table.private.id
#   destination_prefix_list = "com.amazonaws.ap-southeast-2.ecr.dkr"
#   vpc_endpoint_id        = aws_vpc_endpoint.ecr_dkr.id
# }

# resource "aws_route" "ecr_api_route" {
#   route_table_id         = aws_route_table.private.id
#   destination_cidr_block = "com.amazonaws.ap-southeast-2.ecr.api"
#   vpc_endpoint_id        = aws_vpc_endpoint.ecr_api.id
# }

#Separate route table for S3 gateway
# resource "aws_route_table" "s3_gateway" {
#   vpc_id = aws_vpc.my_vpc.id
# }

#configure routing to s3 gateway
# resource "aws_route" "s3_gateway_route" {
#   route_table_id         = aws_route_table.s3_gateway.id
#   destination_cidr_block = "0.0.0.0/0"  # Route all traffic to S3 gateway
#   vpc_endpoint_id        = aws_vpc_endpoint.s3_gateway.id
# }

# enable VPC flow logs
# resource "aws_flow_log" "flow_log" {
#   # Name for the Flow Log
#   name           = "app-engine-vpc-flow-log"

#   # The ID of your VPC
#   vpc_id         = aws_vpc.my_vpc.id # Replace with your VPC ID

#   # The ARN of the IAM role that AWS uses to deliver the flow logs to the chosen log destination
#   iam_role_arn   = "arn:aws:iam::123456789012:role/flow-logs-role" # Replace with your IAM role ARN

#   # The ARN of the CloudWatch Logs Log Group where the Flow Logs will be delivered
#   log_group_name = "/aws/vpc/flow-logs" # Replace with your CloudWatch Log Group ARN

#   # Set the traffic type to "ALL" or specify "ACCEPT" or "REJECT" to filter the logs
#   traffic_type   = "ALL"
# }

data "aws_availability_zones" "available" {}

output "vpc_id" {
  value = aws_vpc.my_vpc.id
}

output "public_subnet_ids" {
  value = aws_subnet.public_subnet[*].id
}

output "private_subnet_ids" {
  value = aws_subnet.private_subnet[*].id
}

output "private_route_table_id" {
  value = aws_route_table.private.id
}