variable "vpc_id" {
  description = "VPC ID"
}
variable "public_subnet_id" {
  description = "Public Subnet ID"
}

variable "private_subnet_id" {
  description = "Public Subnet ID"
}

variable "ecs_agent_role_name" {
  description = "ECS Agent iam role"
}